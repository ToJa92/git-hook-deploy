# Copyright 2014 Tobias Jansson
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from sys import version_info as python_version
import json
import sys
from socket import SHUT_WR
from imp import find_module
if python_version.major > 2:
    from urllib.parse import parse_qs
    from http.server import BaseHTTPRequestHandler, HTTPServer
else:
    from urlparse import parse_qs
    from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer

try:
    find_module('subprocess32')
    from subprocess32 import call
except ImportError:
    from subprocess import call

class Config():
    def __init__(self):
        self.port = None
        self.quiet = False
        self.repositories = []

    def read_config(self, filename):
        with open(filename, 'r') as f:
            try:
                config = json.load(f)
            except ValueError:
                print('FATAL: JSON file is invalid')
                print('Exiting...')
                sys.exit(1)
        missing_directives = []
        invalid_format = []
        try:
            self.port = int(config['port'])
        except KeyError:
            missing_directives.append('port')
        except ValueError:
            invalid_format.append('port')
        try:
            if isinstance(config['quiet'], bool):
                self.quiet = config['quiet']
            else:
                invalid_format.append('quiet')
        except KeyError:
            print('Did not find the directive `quiet\'.')
            print('This means that messages will be printed to the console.')
        try:
            repositories = config['repositories']
        except KeyError:
            missing_directives.append('repositories')
        if isinstance(repositories, list):
            for i in repositories:
                try:
                    self.repositories.append({'path': i['path'],
                                              'url': i['url'],
                                              'commands': i['commands']})
                except KeyError:
                    missing_directives.append('path or url')
                    break
        else:
            invalid_format += 'repositories'
        if missing_directives:
            for i in missing_directives:
                print('FATAL: Missing directive `'+i+'\'')
        if invalid_format:
            for i in invalid_format:
                print('FATAL: Directive `'+i+'\' has an invalid format')

        if missing_directives or invalid_format:
            sys.exit(1)

class GitDeployHandler(BaseHTTPRequestHandler):
    def do_POST(self):
        self.send_response(200)
        self.end_headers()
        # Close request immediately,
        # We don't want the server to wait
        self.request.shutdown(SHUT_WR)
        self.request.close()
        header = self.headers.get('content-type')
        if header == 'application/x-www-form-urlencoded':
            postLength = int(self.headers.get('content-length'))
            postData = self.rfile.read(postLength)
            postData = parse_qs(postData)
            pl = postData.get('payload')
            if pl:
                try:
                    pl = json.loads(pl[0])
                except:
                    print('Error loading payload json, abandoning request...')
                    return
                target_url = pl['canon_url'] + pl['repository']['absolute_url']
                target_repo = self.lookup_url(target_url)
                if target_repo is not None:
                    cmd_line = ";".join(target_repo.get('commands', []))
                    try:
                        ret = call(cmd_line, cwd=target_repo.get('path'),
                                   shell=True)
                    except OSError:
                        ret = -1
                    if ret != 0:
                        print('%s exited with status: %d, aborting...' %
                              (i, ret))
            else:
                print('No payload in received data')
    def do_GET(self):
        self.send_response(503)
        self.send_header('Content-type', 'text/html')
        self.end_headers()
        content = '<html><body><h1>503 - Forbidden</h1></body></html>'
        self.wfile.write(content.encode("UTF-8"))

    # Try to find a `needle' URL in the config
    def lookup_url(self, needle):
        for i in self.server.config.repositories:
            if needle == i['url']:
                # Return the entire dictionary so that the path is known
                return i
        return None

conf = Config()
conf.read_config("config.json")
print('Repos found:')
for i in conf.repositories:
    print('path: '+i['path']+', url: '+i['url'])
httpd = HTTPServer(('', conf.port), GitDeployHandler)
httpd.config = conf
httpd.serve_forever()
