# git-hook-deploy

A simple script that listens to a port
for incoming POST hook requests from [BitBucket](https://bitbucket.org).

## Requirements

* Python 2 *or* Python 3
* (optional) [subprocess32](https://pypi.python.org/pypi/subprocess32/)
* The default shell of the user running the script needs to support `;` as
  a delimiter between the commands to be run. `/bin/sh` supports that type
  of delimiter, so in practice all shells should.

## Example configuration file

The *port* used needs to be open to requests from the internet
(and forwarded properly).

*path* will be the starting directory for your commands.

    {
        "port": 1234,
        "repositories":
        [
                {"path": "/path/to/repository",
                 "url": "http://bitbucket.org/username/repository_name",
                 "commands": [
                         "git pull",
                         "echo pull complete"
                 ]},
        ]
    }

## Usage

    python git-hook-deploy.py

Set up a POST hook for a Git repository on BitBucket, push some changes to the
repository and hopefully the server should execute the commands.